from django.shortcuts import get_object_or_404, render, redirect
from .models import Group, Student, Grade

def dashboard(request):
    groups = Group.objects.all()
    students = Student.objects.all()

    return render(request, 'gradebook/dashboard.html', {'groups': groups, 'students': students})

def groups(request):
    groups = Group.objects.all()
    return render(request, 'gradebook/groups.html', {'groups': groups})

def group(request, groupId):
    group = get_object_or_404(Group, pk=groupId)
    return render(request, 'gradebook/group.html', {'group': group})

def student(request, studentId):
    print(studentId)
    
    currentStudent = get_object_or_404(Student, pk=studentId)
    groups = Group.objects.all()

    for group in groups:
        for student in group.students.all():
            if student.id == currentStudent.id:
                groupId = group.id

    return render(request, 'gradebook/student.html', {'student': currentStudent, 'group': groupId})

def addGroup(request):
    name = request.POST['name']

    group = Group(name=name)
    group.save()

    return redirect('gradebook:dashboard')

def addStudentToGroup(request):
    groupId = request.POST['group']
    studentId = request.POST['student']

    group = get_object_or_404(Group, pk=groupId)
    student = get_object_or_404(Student, pk=studentId)

    if student not in group.students.all():
        group.students.add(student)
        group.save()

    return redirect('gradebook:dashboard')

def addStudent(request):
    firstName = request.POST['firstName']
    lastName = request.POST['lastName']

    student = Student(firstName=firstName, lastName=lastName)
    student.save()
    
    return redirect('gradebook:dashboard')

def addGradesForm(request):
    groupId = request.POST['group']
    group = get_object_or_404(Group, pk=groupId)

    return render(request, 'gradebook/addgrades.html', {'group': group})

def addGradesInGroup(request):
    groupId = request.POST['group']

    for key, value in request.POST.items():
        if key.startswith('grade'):
            studentId = int(key[5:])
            student = Student.objects.get(pk=studentId)
            grade = Grade(value=int(value))
            grade.save()
            student.grades.add(grade)
            student.save()

    return redirect('gradebook:group', groupId)

def addGrade(request):
    studentId = request.POST['student']
    value = request.POST['grade']

    student = get_object_or_404(Student, pk=studentId)
    grade = Grade(value=int(value))
    grade.save()
    student.grades.add(grade)
    student.save()

    return redirect('gradebook:student', studentId)

def changeGrade(request):
    studentId = request.POST['student']
    oldGrade = int(request.POST['grade'])
    newGrade = request.POST['newGrade']

    student = get_object_or_404(Student, pk=studentId)

    for grade in student.grades.all():
        if grade.id == oldGrade:
            grade.value = newGrade
            grade.save()
    
    student.save()

    return redirect('gradebook:student', studentId)
