from django.db import models
from datetime import datetime

class Grade(models.Model):
    value = models.IntegerField(default=2)
    date = models.DateTimeField(default=datetime.now)

class Student(models.Model):
    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    grades = models.ManyToManyField(Grade)

class Group(models.Model):
    name = models.CharField(max_length=100)
    students = models.ManyToManyField(Student)

