from django.urls import path

from . import views

app_name = 'gradebook'
urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('groups', views.groups, name='groups'),
    path('groups/<int:groupId>/', views.group, name='group'),
    path('students/<int:studentId>', views.student, name='student'),
    path('groups/add', views.addGroup, name='addGroup'),
    path('students/add', views.addStudent, name='addStudent'),
    path('groups/students/add', views.addStudentToGroup, name='addStudentToGroup'),
    path('groups/grades', views.addGradesForm, name='addGradesForm'),
    path('groups/grades/add', views.addGradesInGroup, name='addGradesInGroup'),
    path('students/grades/add', views.addGrade, name='addGrade'),
    path('students/grades/change', views.changeGrade, name='changeGrade'),
]
